# Get started:
Alle Notebooks wurden mit Anaconda ausgeführt. um das Anaconda Environment zu kopieren öffnen Sie ihre Anaconda Command Prompt und geben Sie `conda create --name myenv --file path/to/conda.txt` ein. Die Datei finden Sie in diesem Ordner.
## Datenverarbeitung
Um die Datenverarbeitung auszuführen, downloaden Sie bitte [hier](https://drive.google.com/drive/folders/1mSLgwVTiaUMAb4AVOWwlCD5JcWdrwpvu) den Datensatz driver_23_30frame.tar.gz und entpacken ihn in ./Datensaetze.
Anschließend kann das Jupyer Notebook Datenverarbeitung ausgeführt werden.
Die dafür notwendigen dateien befinden sich in *./data_compressed* und in *./Datensaetze*.  
Alle weiteren Anweisungen sind dem Notebook zu entnehmen.

---
## Modell Training
Um ein Model zu trainieren, können die Daten aus der Datenverarbeitung oder die vorbereiteten Daten in *./processed_imgs* und *./labels* genutzt werden.
Es ist also nicht notwendig die Datenverarbeitung vorher durchzuführen


