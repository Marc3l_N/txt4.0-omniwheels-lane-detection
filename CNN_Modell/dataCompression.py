import numpy as np
import cv2 #Bibliothek zur Bildbearbeitung
class PackPics:
    def importPics(img_paths:list, pathname:str):
        width = int(240)
        height = int(360)
        dim = (width, height)

        #f= IntProgress(min=0, max=max_count)
        #display(f)
        imgs=np.empty([0,height, width]) #leeres numpy array für Bilder erstellen
        for path in img_paths: #tqdm zeigt Fortschritt an
            resized_img = cv2.resize(cv2.imread(path,0), dim) #  die 0 in cv2.imread steht für schwarz-weiß einlesen
            imgs= np.append(imgs,[resized_img],axis=0) 
        np.save(pathname, imgs)

    def packArrays(img_paths:list, pathname:str,number_of_imgs :int,width:int, height:int):
        imgsfull=np.empty([0,height, width])
        for paths in img_paths:
            np.append(imgsfull,np.load(paths,allow_pickle=True),axis=0)
        np.save(pathname, imgsfull)

        
        