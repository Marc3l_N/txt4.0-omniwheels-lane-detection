from fischertechnik.controller.Motor import Motor
from lib.controller import *
import cv2

def turn(degrees, direction):
    #4700 werte wurden experimentell entwickelt. Genauigkeit 90%
    angle= int(degrees/360*4700)
    if direction== "r":
        for count in range(angle):
            TXT_M_M1_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M1_encodermotor.set_distance(int(65536))
            TXT_M_M2_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M2_encodermotor.set_distance(int(65536))
            TXT_M_M3_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M3_encodermotor.set_distance(int(65536))
            TXT_M_M4_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M4_encodermotor.set_distance(int(65536))
    elif direction== "l":
        for count in range(angle):
            TXT_M_M1_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M1_encodermotor.set_distance(int(65536))
            TXT_M_M2_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M2_encodermotor.set_distance(int(65536))
            TXT_M_M3_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M3_encodermotor.set_distance(int(65536))
            TXT_M_M4_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M4_encodermotor.set_distance(int(65536))
def moveparallel(direction, distance):
    if direction== "r":
        for count in range(int(4700/2)):
            #range 15000= 1m 
            TXT_M_M1_encodermotor.set_speed(int(195), Motor.CCW)
            TXT_M_M1_encodermotor.set_distance(int(65536))
            TXT_M_M2_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M2_encodermotor.set_distance(int(65536))
            TXT_M_M3_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M3_encodermotor.set_distance(int(65536))
            TXT_M_M4_encodermotor.set_speed(int(195), Motor.CW)
            TXT_M_M4_encodermotor.set_distance(int(65536))

    if direction== "l":
        for count in range(int(4700/2)):
            
            TXT_M_M1_encodermotor.set_speed(int(190), Motor.CW)
            TXT_M_M1_encodermotor.set_distance(int(65536))
            TXT_M_M2_encodermotor.set_speed(int(200), Motor.CW)
            TXT_M_M2_encodermotor.set_distance(int(65536))
            TXT_M_M3_encodermotor.set_speed(int(200), Motor.CCW)
            TXT_M_M3_encodermotor.set_distance(int(65536))
            TXT_M_M4_encodermotor.set_speed(int(190), Motor.CCW)
            TXT_M_M4_encodermotor.set_distance(int(65536))
def getframe():
    img= TXT_M_USB1_1_camera.read_frame()
    #TODO check if img= BGR
    img= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #make picture gray 
    img= cv2.resize(img,dsize=(164,59))
    img = img/255
    img = img[23 :]

