import fischertechnik.factories as txt_factory

txt_factory.init()
txt_factory.init_input_factory()
txt_factory.init_motor_factory()
txt_factory.init_usb_factory()
txt_factory.init_camera_factory()

TXT_M = txt_factory.controller_factory.create_graphical_controller()
TXT_M_I7_color_sensor = txt_factory.input_factory.create_color_sensor(TXT_M, 7)
TXT_M_I8_color_sensor = txt_factory.input_factory.create_color_sensor(TXT_M, 8)
TXT_M_M1_encodermotor = txt_factory.motor_factory.create_encodermotor(TXT_M, 1)
TXT_M_M2_encodermotor = txt_factory.motor_factory.create_encodermotor(TXT_M, 2)
TXT_M_M3_encodermotor = txt_factory.motor_factory.create_encodermotor(TXT_M, 3)
TXT_M_M4_encodermotor = txt_factory.motor_factory.create_encodermotor(TXT_M, 4)
TXT_M_USB1_1_camera = txt_factory.usb_factory.create_camera(TXT_M, 1)

txt_factory.initialized()